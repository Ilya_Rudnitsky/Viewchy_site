package com.viewchy.model;

public enum Type {
    OUTWEAR(1), TSHIRT(2), PANTS(3), SWEATSHIRT(4), SHIRT(5);

    private int index;

    private Type(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}