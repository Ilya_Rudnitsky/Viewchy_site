package com.viewchy.model;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Product {

    private Integer id;
    private Type type;
    private BigDecimal price;
    private String name;
    private Map<Color, String[]> colors = new HashMap<>();

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void addColor(Color color, String[] files) {
        colors.put(color, files);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (id != null ? !id.equals(product.id) : product.id != null) return false;
        if (type != product.type) return false;
        if (price != null ? !price.equals(product.price) : product.price != null) return false;
        if (name != null ? !name.equals(product.name) : product.name != null) return false;
        return colors != null ? colors.equals(product.colors) : product.colors == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (colors != null ? colors.hashCode() : 0);
        return result;
    }
}
