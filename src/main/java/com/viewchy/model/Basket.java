package com.viewchy.model;

import javafx.util.Pair;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Basket {

    private Map<Product, Integer> products = new HashMap<>();
    private BigDecimal totalPrice = BigDecimal.ZERO;
    private int totalCount = 0;

    public Map<Product, Integer> getProducts() {
        return new HashMap<>(products);
    }

    public BigDecimal addProduct(Product product) {
        if(product == null) {
            return new BigDecimal(totalPrice.doubleValue());
        }

        Integer count = products.get(product);
        if(count == null) {
            count = 0;
        }
        products.put(product, count + 1);
        totalCount ++;
        totalPrice = totalPrice.add(product.getPrice());
        return totalPrice;
    }

    public BigDecimal decreaseProduct(Product product) {
        if(product == null) {
            return new BigDecimal(totalPrice.doubleValue());
        }

        Integer count = products.get(product);
        if(count == null) {
            return new BigDecimal(totalPrice.doubleValue());
        }

        if(--count <= 0) {
            products.remove(product);
        }
        else {
            products.put(product, count);
        }
        totalCount --;
        totalPrice = totalPrice.subtract(product.getPrice());
        return totalPrice;
    }

    public BigDecimal getTotalPrice() {
        return new BigDecimal(totalPrice.doubleValue());
    }

    public Integer getProductAmount(Product product) {
        return products.get(product);
    }

    public List<Pair> asPairs() {
        return products.entrySet().stream().map(this::toPair).collect(Collectors.toList());
    }

    private Pair toPair(Map.Entry<Product, Integer> entry) {
        return new Pair<Product, Integer>(entry.getKey(), entry.getValue());
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void clean() {
        products.clear();
        totalCount = 0;
        totalPrice = BigDecimal.ZERO;
    }

    public void removeProduct(Product product) {
        Integer count = products.get(product);
        totalCount -= count;
        totalPrice = totalPrice.subtract(product.getPrice().multiply(new BigDecimal(count)));
        products.remove(product);
    }
}
