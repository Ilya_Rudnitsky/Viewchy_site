package com.viewchy.model;

public enum Color {
    BLACK(1), BLUE(2), PINK(3), YELLOW(4), WHITE(5), GRAY(6), RED(7), GREEN(8);

    private int index;

    private Color(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
