package com.viewchy.transaction;

import com.viewchy.repos.RepositoryAccessWrapper;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Dmytro_Ponomar on 8/16/2017.
 */
public final class TransactionManager {

    private static DataSource dataSource;

    private TransactionManager(){}

    static {
        try {
            InitialContext initContext = new InitialContext();
            dataSource = (DataSource) initContext.lookup("java:comp/env/jdbc/database");
        }
        catch (NamingException e) {
            throw new IllegalStateException(e);
        }
    }

    public static Object doTransaction(RepositoryAccessWrapper wrapper) {
        try(Connection con = dataSource.getConnection()) {
            try {
                con.setAutoCommit(false);
                Object result = wrapper.invoke(con);
                con.commit();

                return result;
            } catch (SQLException e) {
                con.rollback();
                throw e;
            }
        }
        catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
