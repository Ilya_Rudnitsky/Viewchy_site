package com.viewchy.facade;

import com.viewchy.model.Product;
import com.viewchy.model.Type;
import com.viewchy.repos.ProductRepository;
import com.viewchy.repos.impl.ProductRepositoryImpl;
import com.viewchy.service.ProductRepositoryService;
import com.viewchy.service.impl.ProductRepositoryServiceImpl;
import com.viewchy.util.ProductCache;

import java.util.List;

/**
 * Created by Dmytro_Ponomar on 8/4/2017.
 */
public final class Facade {

    private static ProductRepository productRepository;
    private static ProductRepositoryService productRepositoryService;
    private static ProductCache cache;

    static {

        productRepository = new ProductRepositoryImpl();
        productRepositoryService = new ProductRepositoryServiceImpl();
        cache = new ProductCache();

    }

    private Facade(){}

    public static ProductRepository getProductRepository() {return productRepository;}

    public static List<Product> getRandomProducts(int count) {
        return productRepositoryService.getRandomProducts(count);
    }

    public static List<Product> getProductsByType(Type type) {
        return productRepositoryService.getProductsByType(type);
    }

    public static Product getProductById(int id) {
        return productRepositoryService.getProductById(id);
    }

    public static Product getCacheProduct(int id) {
        return cache.get(id);
    }

}
