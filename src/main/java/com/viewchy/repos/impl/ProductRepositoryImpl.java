package com.viewchy.repos.impl;

import com.viewchy.model.Color;
import com.viewchy.model.Product;
import com.viewchy.model.Type;
import com.viewchy.repos.ProductRepository;
import com.viewchy.util.Util;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmytro_Ponomar on 8/16/2017.
 */
public class ProductRepositoryImpl implements ProductRepository {

    private static final String PATH_TO_IMAGES = "src/main/webapp/img/store";
    private static final String SLASH = "/";

    @Override
    public List<Product> getProductsByType(Connection con, Type type) throws SQLException {
        List<Product> products = new ArrayList<>();
        String request = Util.getQuery("products.getByType");

        PreparedStatement stat = con.prepareStatement(request);
        stat.setInt(1, type.getIndex());
        ResultSet set = stat.executeQuery();

        while (set.next()) {
            products.add(createProduct(set));
        }

        return products;
    }

    @Override
    public Product getProductById(Connection con, int id) throws SQLException {
        String request = Util.getQuery("products.getById");
        String colorsRequest  = Util.getQuery("products.getColors");

        PreparedStatement stat = con.prepareStatement(request);
        stat.setInt(1, id);
        ResultSet set = stat.executeQuery();

        PreparedStatement colorsStat = con.prepareStatement(colorsRequest);
        colorsStat.setInt(1, id);
        ResultSet colorsSet = colorsStat.executeQuery();

        Product product = null;

        while (set.next()) {
           product = createProduct(set);
        }

        setColorsToProduct(product, colorsSet);
        return product;
    }

    @Override
    public List<Product> getRandomProducts(Connection con, int count) throws SQLException{
        List<Product> products = new ArrayList<>();
        String request = Util.getQuery("products.getRand");
        PreparedStatement stat = con.prepareStatement(request);
        stat.setInt(1, count);
        ResultSet set = stat.executeQuery();

        while (set.next()) {
            products.add(createProduct(set));
        }

        return products;
    }

    private Product createProduct(ResultSet set) throws SQLException{
        Product product = new Product();
        int i = 1;
        product.setId(set.getInt(i++));
        product.setName(set.getString(i++));
        product.setPrice(set.getBigDecimal(i++));
        product.setType(Type.values()[set.getInt(i) - 1]);
        return  product;
    }

    private void setColorsToProduct(Product product, ResultSet resultSet) throws SQLException{
        StringBuilder pathToDir = new StringBuilder();
        while (resultSet.next()) {
            pathToDir.setLength(0);
            Color color = Color.values()[resultSet.getInt(1) - 1];
            String colorStr = color.toString().toLowerCase();
            pathToDir.append(PATH_TO_IMAGES)
                    .append(SLASH).append(product.getType().toString().toLowerCase())
                    .append(SLASH).append(product.getId())
                    .append(SLASH).append(colorStr);
            File file = new File(pathToDir.toString());

            if(!file.exists() || !file.isDirectory()) {
                continue;
            }

            product.addColor(color, file.list());
        }
    }
}
