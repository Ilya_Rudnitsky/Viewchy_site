package com.viewchy.repos;

import com.viewchy.model.Product;
import com.viewchy.model.Type;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Dmytro_Ponomar on 8/16/2017.
 */
public interface ProductRepository {

    List<Product> getRandomProducts(Connection con, int count) throws SQLException;

    List<Product> getProductsByType(Connection con, Type type) throws SQLException;

    Product getProductById(Connection con, int id) throws SQLException;

}
