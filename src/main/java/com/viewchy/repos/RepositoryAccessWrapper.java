package com.viewchy.repos;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Dmytro_Ponomar on 8/11/2017.
 */
public interface RepositoryAccessWrapper {

    Object invoke(Connection connection) throws SQLException;

}
