package com.viewchy.servlet;

import com.google.gson.Gson;
import com.viewchy.facade.Facade;
import com.viewchy.model.Product;
import com.viewchy.model.Type;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/getProductsByType")
public class ProductsByTypeServlet extends HttpServlet {

    private static final Gson GSON = new Gson();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        String typeParam = req.getParameter("type");


        Type type;
        try {
            type = Type.valueOf(typeParam.toUpperCase());
        } catch (Exception e) {
            resp.getWriter().write(GSON.toJson(new Product()));
            resp.getWriter().flush();
            return;
        }

        List<Product> products = Facade.getProductsByType(type);
        String jsonList = GSON.toJson(products);

        resp.getWriter().write(jsonList);
        resp.getWriter().flush();

    }
}
