package com.viewchy.servlet;

import com.google.gson.Gson;
import com.viewchy.model.Basket;
import com.viewchy.util.Util;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

@WebServlet("/getMiniBasket")
public class GetMiniBasketServlet extends HttpServlet{

    private static final Gson GSON = new Gson();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        Basket basket = Util.getBasketFromSession(req.getSession());
        Pair<BigDecimal, Integer> pair = new Pair<>(basket.getTotalPrice(), basket.getTotalCount());

        resp.getWriter().write(GSON.toJson(pair));
        resp.getWriter().flush();
    }
}
