package com.viewchy.servlet;

import com.google.gson.Gson;
import com.viewchy.facade.Facade;
import com.viewchy.model.Basket;
import com.viewchy.util.BasketUpdateHelper;
import com.viewchy.util.Util;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

@WebServlet("/removeProduct")
public class RemoveProductFromBasketServlet extends HttpServlet {

    private static final String ID = "id";
    private static final Gson GSON = new Gson();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        Basket basket = Util.getBasketFromSession(req.getSession());
        String idParam = req.getParameter(ID);

        int id;
        try {
            id = Integer.parseInt(idParam);
        } catch (Exception e) {
            resp.getWriter().write(GSON.toJson(basket.getTotalPrice()));
            resp.getWriter().flush();
            return;
        }

        basket.removeProduct(Facade.getCacheProduct(id));

        BasketUpdateHelper updateHelper = new BasketUpdateHelper();
        updateHelper.setTotalPrice(basket.getTotalPrice());
        updateHelper.setCount(basket.getTotalCount());
        updateHelper.setId(id);

        resp.getWriter().write(GSON.toJson(updateHelper));
        resp.getWriter().flush();

    }
}
