package com.viewchy.servlet;

import com.google.gson.Gson;
import com.viewchy.facade.Facade;
import com.viewchy.model.Basket;
import com.viewchy.util.Util;
import javafx.util.Pair;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

@WebServlet("/addToBasket")
public class AddToBasketServlet extends HttpServlet {

    private static final String ID = "id";
    private static final Gson GSON = new Gson();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        Basket basket = Util.getBasketFromSession(req.getSession());
        String idParam = req.getParameter(ID);

        int id;
        try {
            id = Integer.parseInt(idParam);
        } catch (Exception e) {
            resp.getWriter().write(GSON.toJson(basket.getTotalPrice()));
            resp.getWriter().flush();
            return;
        }

        BigDecimal totalPrice = basket.addProduct(Facade.getCacheProduct(id));
        Pair<BigDecimal, Integer> pair = new Pair<>(totalPrice, basket.getTotalCount());

        resp.getWriter().write(GSON.toJson(pair));
        resp.getWriter().flush();
    }
}
