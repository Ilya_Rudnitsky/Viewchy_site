package com.viewchy.servlet;

import com.google.gson.Gson;
import com.viewchy.facade.Facade;
import com.viewchy.model.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.*;

@WebServlet("/getRandomProducts")
public class AdvertisingServlet extends HttpServlet {

    private static final Gson GSON = new Gson();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        String countParam = req.getParameter("count");
        if (isEmpty(countParam)) {
            resp.getWriter().write(GSON.toJson(new Product()));
            resp.getWriter().flush();
            return;
        }

        Integer count = Integer.parseInt(countParam);

        List<Product> products = Facade.getRandomProducts(count);
        String jsonList = GSON.toJson(products);

        resp.getWriter().write(jsonList);
        resp.getWriter().flush();
    }

}
