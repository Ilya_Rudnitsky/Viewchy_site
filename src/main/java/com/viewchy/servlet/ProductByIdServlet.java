package com.viewchy.servlet;

import com.google.gson.Gson;
import com.viewchy.facade.Facade;
import com.viewchy.model.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/getProductsById")
public class ProductByIdServlet extends HttpServlet {

    private static final Gson GSON = new Gson();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        String idParam = req.getParameter("id");

        int id;
        try {
           id = Integer.parseInt(idParam);
        } catch (Exception e) {
            resp.getWriter().write(GSON.toJson(new Product()));
            resp.getWriter().flush();
            return;
        }

        resp.getWriter().write(GSON.toJson(Facade.getCacheProduct(id)));
        resp.getWriter().flush();

    }
}
