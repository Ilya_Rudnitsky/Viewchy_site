package com.viewchy.service;

import com.viewchy.model.Product;
import com.viewchy.model.Type;

import java.util.List;

/**
 * Created by Dmytro_Ponomar on 8/16/2017.
 */
public interface ProductRepositoryService {

    List<Product> getRandomProducts(int count);

    List<Product> getProductsByType(Type type);

    Product getProductById(int id);
}
