package com.viewchy.service.impl;


import com.viewchy.facade.Facade;
import com.viewchy.model.Product;
import com.viewchy.model.Type;
import com.viewchy.service.ProductRepositoryService;
import com.viewchy.transaction.TransactionManager;

import java.sql.Connection;
import java.util.List;

/**
 * Created by Dmytro_Ponomar on 8/16/2017.
 */
public class ProductRepositoryServiceImpl implements ProductRepositoryService {

    @Override
    public List<Product> getRandomProducts(int count) {
        return (List<Product>) TransactionManager.doTransaction((Connection connection) -> Facade.getProductRepository().getRandomProducts(connection, count));
    }

    @Override
    public List<Product> getProductsByType(Type type) {
        return (List<Product>) TransactionManager.doTransaction((Connection connection) -> Facade.getProductRepository().getProductsByType(connection, type));
    }

    @Override
    public Product getProductById(int id) {
        return (Product) TransactionManager.doTransaction((Connection con) -> Facade.getProductRepository().getProductById(con, id));
    }
}
