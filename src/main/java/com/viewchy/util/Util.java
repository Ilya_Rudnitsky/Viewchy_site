package com.viewchy.util;

import com.viewchy.model.Basket;

import javax.servlet.http.HttpSession;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Dmytro_Ponomar on 8/3/2017.
 */
public final class Util {

    private static final String BASKET = "basket";

    private Util(){}

    public static String getQuery(String key) {
        FileInputStream fis;
        Properties property = new Properties();

        try {
            fis = new FileInputStream("src/main/resources/DBQueries.properties");
            property.load(fis);
            return property.getProperty(key);
        }
        catch (IOException e) {
            throw  new IllegalStateException(e);
        }
    }

    public static Basket getBasketFromSession(HttpSession session) {
        Basket basket = (Basket)session.getAttribute(BASKET);
        if(basket == null) {
            basket = new Basket();
            session.setAttribute(BASKET, basket);
        }
        return basket;
    }
}
