package com.viewchy.util;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.viewchy.facade.Facade;
import com.viewchy.model.Product;

import java.util.concurrent.TimeUnit;

public class ProductCache {

    private LoadingCache<Integer, Product> cache;

    public  ProductCache() {
        cache = CacheBuilder.newBuilder().expireAfterAccess(10, TimeUnit.MINUTES).build(new CacheLoader<Integer, Product>() {
            @Override
            public Product load(Integer id) throws Exception {
                return Facade.getProductById(id);
            }
        });
    }

    public Product get(int id) {
        return cache.getUnchecked(id);
    }

}
