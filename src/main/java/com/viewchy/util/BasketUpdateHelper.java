package com.viewchy.util;

import java.math.BigDecimal;

/**
 * Created by Dmytro_Ponomar on 8/29/2017.
 */
public class BasketUpdateHelper {

    private int id;
    private Integer count;
    private BigDecimal totalPrice;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }
}
