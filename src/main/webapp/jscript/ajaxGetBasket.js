$(document).ready(function () {

    var object = "";
    var id;
    var val;

    var basketCount = $("#count");
    var basketQuantity = $("#valueBasket");

    $.get("/getBasket", function (data, status){

        for(var i = 0; i < data.length; i++){
            object += '<div class="addedToBasketProduct addedToBasketProduct' + data[i].key.id +'"> ' +
                '<img src="../img/store/' + data[i].key.type.toLowerCase() + '/' + data[i].key.id + '/main.png" alt="product"> ' +
                '<div class="description"> ' +
                '<h1>'+ data[i].key.name +'</h1> ' +
                '<h2 id="price">$'+ data[i].key.price +'</h2> ' +
                '</div>' +
                '<div class="number"> ' +
                '<h3>Select quantity</h3> ' +
                '<a href="#" id="minus" class="minus">-</a> ' +
                '<input id="quantityField" placeholder="" type="text" value="' + data[i].value +  '" class="' + data[i].key.id + '" disabled> ' +
                '<a href="#" id="plus" class="plus">+</a> ' +
                '</div>' +
                '<div class="delete delete' + data[i].key.id + '">&#215;</div> ' +
                '</div>' +
                '<input type="hidden" value = '+ data[i].key.id +' class="input">';
        }

        $(".content").html(object);

        $("img").click(function (event) {
           document.location.href = 'productDetail.html?id='+ $(this).attr('src').match(/\d/g).join('') +'';
        });


        $(".plus").click(function (event) {
            event.preventDefault();

            var prevObj = $(this).prev();

            id = +prevObj.attr('class');
            val = prevObj.val();
            prevObj.val(++val);

            $.post("/increaseQuantity", {id: id}, function (data) {
                $(this).prev().val(id);
            });

            $.get("/getMiniBasket", function (data, status) {

                basketCount.text("$" + data.key);
                basketQuantity.text(data.value);
            });

        });



        $(".minus").click(function (event){
            event.preventDefault();

            id = +$(this).next().attr('class');
            val = $(this).next().val();

            if(val > 1){
                $(this).next().val(--val);

            } else {
                var objdlt = ".addedToBasketProduct" + id;
                var dlt = $(".content").find(objdlt);
                $(dlt).remove();
                $(".empty").html("<h2>Your basket empty</h2>");
            }

            $.post("/decreaseQuantity", {id: id}, function () {
                $(this).next().val(id);
            });

            $.get("/getMiniBasket", function (data, status) {
                basketCount.text("$" + data.key);
                basketQuantity.text(data.value);
            });

        });

        $(".delete").click(function (event) {
            event.preventDefault();
            var conf = confirm('Are you wanting to delete product from basket?');
            if(conf === true){
                id = $(this).attr('class').match(/\d/g).join('');


                $.post("/removeProduct", {id: id}, function (data, status) {
                    var objdlt = ".addedToBasketProduct" + id.match(/\d/g).join('');
                    $(objdlt).remove();
                    $(".empty").html("<h2>Your basket empty</h2>");
                    basketCount.text("$" + data.totalPrice);
                    basketQuantity.text(data.count);

                });

            } else {
                $(".empty").html("");
            }

        });

        var htmlContent = $(".content").html();

        if(htmlContent === ""){
            $(".empty").html("<h2>Your basket empty</h2>");

        } else {
            $(".empty").html("");
        }



    });
});

