$(document).ready(function () {

    var field = $("#advertising_content");
    var imgs = ['slide1', 'slide2', 'slide3', 'slide4'];
    var i = 0;

    $(".next").click(function () {

        if (i !== 3) {
            field.removeClass(imgs[i]);
            field.addClass(imgs[i + 1]);
            i++;
        } else {
            field.removeClass(imgs[3]);
            field.addClass(imgs[0]);
            i = 0;
        }

    });

    $(".prev").click(function () {

        if (i === 0) {
            field.removeClass(imgs[i]);
            i = 3;
            field.addClass(imgs[i]);
        } else {
            field.removeClass(imgs[i]);
            field.addClass(imgs[i - 1]);
            i--;
        }

    });


    setInterval(function () {

        if (i !== 3) {
            field.removeClass(imgs[i]);
            field.addClass(imgs[i + 1]);
            i++;
        } else {
            field.removeClass(imgs[3]);
            field.addClass(imgs[0]);
            i = 0;
        }
    }, 6000);

    // field.animate({background: "100px"})

});