$(document).ready(function (){

    var quantityField =$("#quantityField");
    var quantityFieldVal = quantityField.val();

    $("#plus").click(function (event) {
        event.preventDefault();
        quantityField.val(++quantityFieldVal);
    });

    $("#minus").click(function (event) {
        event.preventDefault();
        if(quantityFieldVal > 1){
            quantityField.val(--quantityFieldVal);
        }
    });
});