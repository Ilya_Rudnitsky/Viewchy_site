$(document).ready(function (){
    var mainLink = document.location.href;
    var ans = mainLink.search("id=");
    var link = mainLink.substring(ans+3);

    var objPDP = "";
    var miniPhoto = "";
    var clr = [];
    var indexCLR;

    $.get("/getProductsById?id="+link, function (data) {

        for(var k in data.colors){
            clr.push(k);
        }

        objPDP = '<div class="productDetail"> ' +
            '<img src="../img/store/' + data.type.toLowerCase() + '/' + data.id + '/main.png" class="mainPhoto"> ' +
            '<input type="hidden" value="' + data.id + '" id="input">' +
            '</div> ' +
            '<div class="textProductDetail"> ' +
            '<h1>' + data.name + '</h1> ' +
            '<h2>$' + data.price +'</h2> ' +
            '<a href="#" id="but">Add to basket</a> ' +
            '</div> ' +
            '<div class="prePictureChooser">' +
            '<div class="pictureChooser">' +
            '</div>' +
            '</div>';

        $(".content").html(objPDP);

        $("#but").click(function (event) {
            event.preventDefault();
            var id = $("#input").val();
            var count = $("#count").text();
            var quantyti = $("#valueBasket");

            $.post("/addToBasket",{id: id}, function (data, status) {

                if(count === ""){
                    $("#count").text("$0");
                } else {
                    $("#count").text("$" + data.key);
                }
                quantyti.text(data.value);

            });
        });
    });
});


// ********************************ДЛЯ МИНИ КАРТИНОК*******************************

// $(".pictureChooser").remove();
// for(var i = 0; i < data.colors.YELLOW.length; i++){
//     miniPhoto += '<img src="../img/store/' + data.type.toLowerCase() + '/' + data.id + '/' + clr[0].toLowerCase() + '/' + data.colors.YELLOW[i] + '">';
// }
// $(".prePictureChooser").html('<div class="pictureChooser"> ' + miniPhoto + '</div>');

// ********************************ДЛЯ МИНИ КАРТИНОК*******************************
