$(document).ready(function () {




    $("#cleanButton").click(function (event) {
        event.preventDefault();

        var conf = confirm('Are you sure want to clean basket?');
        if(conf === true){
            $.get("/cleanBasket", function (data, status) {
                $(".empty").html("<h2>Your basket empty</h2>");
                $(".content").remove();
                $("#valueBasket").text('0');
                $("#count").text('$0')
            });
        }
    });
});
