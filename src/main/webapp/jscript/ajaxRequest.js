$(document).ready(function () {

    var advirsObject = "";
    var contentObject = "";

    $.get("/getRandomProducts?count=9", function (data){

        for(var i = 0; i < 3; i++){

            advirsObject += "<div class=\"col-4 contentInAdvertising\">" +
                "<img src=\"img/store/" + data[i].type.toLowerCase() + "/" + data[i].id + "/main.png\" class=\"imgProductOnHome\">" +
                "<div class=\"text\">" +
                "<h3>" + data[i].name +"</h3>" +
                "<a href=\"pages/productDetail.html?id=" + data[i].id + "\">Buy</a>" +
                "</div>" +
                "</div>";

        }

        for(var j = 3; j < data.length; j++){

            contentObject += "<div class=\"product\"> " +
                "<img src=\"img/store/"+ data[j].type.toLowerCase() +"/"+ data[j].id +"/main.png\"> " +
                "<div class=\"nameAndPrice\" > " +
                "<h2>" + data[j].name +"</h2> " +
                "<h3>$" + data[j].price + "</h3> " +
                "</div> " +
                "<a href=\"pages/productDetail.html?id=" + data[j].id + "\">Buy now</a> " +
                "</div>"

        }

        $("#object").html(advirsObject);
        $("#row").html(contentObject);
    });


});
